package testcases;

import org.testng.annotations.Test;

import design.ProjectMethods;
import pages.HomePage;

public class TC002_CreateLead extends ProjectMethods {

	@Test
	public void Login(String uName, String pwd) {

		new HomePage().clickCRMSFA().clickLeads().clickCreateLeads().
		typeCompanyName().typeFirstName().typeLastName()
		.clickCreateLeadButton().viewLeads().returnToHomePage();
	}

}