package testcases;

import org.testng.annotations.Test;

import design.ProjectMethods;
import pages.LoginPage;

public class TC003_MergeLead extends ProjectMethods{

	@Test
	public void mergeLead() {
		new LoginPage().typeUserName("Demosalesmanager").typePassword("crmsfa").clickLogin()
		.clickCRMSFA().clickLeads().clickMergeLeads().clickLookUpLead1()
		.typeLeadIdOfRequiredLead1().clickFindLeads().clickMatchedLead().clickLookUpLead2()
		.typeLeadIdOfRequiredLead2().clickFindLeads().clickMatchedLead().clickMergeButton()
		.clickConfirmAlert().displayLead().returnToHomePage().clickLogOut();
	}
}
