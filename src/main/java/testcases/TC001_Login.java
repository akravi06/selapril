package testcases;

import org.testng.annotations.Test;

import design.ProjectMethods;
import pages.LoginPage;

public class TC001_Login extends ProjectMethods {
	
	
	@Test
	public void Login() {
		new LoginPage()
		.typeUserName("DemoSalesManager")
		.typePassword("crmsfa")
		.clickLogin().clickCRMSFA().clickLeads();
	}

}