package pages;

import org.openqa.selenium.WebElement;
import design.ProjectMethods;

public class CreateLeads extends ProjectMethods {
	
	static WebElement cName = driver.findElementById("createLeadForm_companyName");
	static WebElement fName = driver.findElementById("createLeadForm_firstName");
	static WebElement lName = driver.findElementById("createLeadForm_lastName");
	
	public CreateLeads typeCompanyName() {
		cName.sendKeys("Aaragi & Co.");
		return this;
	}
	
	public CreateLeads typeFirstName() {
		fName.sendKeys("Araragi");
		return this;
	}
	
	public CreateLeads typeLastName() {
		fName.sendKeys("Koyomi");
		return this;
	}
	
	public ViewLead clickCreateLeadButton() {
		driver.findElementByClassName("submitButton").click();
		return new ViewLead();
	}
}