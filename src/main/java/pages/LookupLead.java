package pages;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import design.ProjectMethods;

public class LookupLead extends ProjectMethods{
	
	public static void switchToFindLeadWindow() {
		Set<String> windowHandles = driver.getWindowHandles();
		List<String> windows = new ArrayList<String>();
		windows.addAll(windowHandles);
		driver.switchTo().window(windows.get(1));
	}
	
	public static void switchToDefaultWindow() {
		Set<String> windowHandles = driver.getWindowHandles();
		List<String> windows = new ArrayList<String>();
		windows.addAll(windowHandles);
		driver.switchTo().window(windows.get(0));
	}
	
	public LookupLead typeLeadIdOfRequiredLead1() {
		switchToFindLeadWindow();
		driver.findElementByXPath("//input[@name='id']").sendKeys("10172");
		return this;
	}
	
	public LookupLead typeLeadIdOfRequiredLead2() {
		switchToFindLeadWindow();
		driver.findElementByXPath("//input[@name='id']").sendKeys("10170");
		return this;
	}
	
	public LookupLead clickFindLeads() {
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		return this;
	}
	
	public MergeLeads clickMatchedLead() {
		driver.findElementByXPath("(//a[@class='linktext'])[1]").click();
		return new MergeLeads();
	}

}
