package pages;

import design.ProjectMethods;

public class ViewLead extends ProjectMethods {

	public ViewLead viewLeads() {
		String viewFirstName = driver.findElementById("viewLead_firstName_sp").getText();
		if(CreateLeads.fName.getText().equals(viewFirstName))
			System.out.println("Entry Matched!");
		else
			System.out.println("Entry not Matched!");
		return this;
	}
	
	public ViewLead displayLead() {
		System.out.println("Company Name - "+ driver.findElementByXPath("viewLead_companyName_sp").getText());
		System.out.println("First Name - "+ driver.findElementByXPath("viewLead_firstName_sp").getText());
		System.out.println("Last Name - "+ driver.findElementByXPath("viewLead_lastName_sp").getText());
		return this;
	}
	
	public HomePage returnToHomePage() {
		driver.findElementByXPath("//img[@alt='opentaps CRM']").click();
		return new HomePage();
	}
}