package pages;

import design.ProjectMethods;

public class MyLeads extends ProjectMethods {

	public CreateLeads clickCreateLeads() {
		driver.findElementByXPath("//a[text()='Create Lead']").click();
		return new CreateLeads();
	}
	
	public MergeLeads clickMergeLeads() {
		driver.findElementByLinkText("Merge Leads").click();
		return new MergeLeads();
	}
}