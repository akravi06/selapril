package pages;

import org.openqa.selenium.Alert;

import design.ProjectMethods;

public class MergeLeads extends ProjectMethods{

	public LookupLead clickLookUpLead1() {
		driver.findElementByXPath("(//img[@alt='Lookup'])[1]").click();
		return new LookupLead();
	}

	public LookupLead clickLookUpLead2() {
		LookupLead.switchToDefaultWindow();
		driver.findElementByXPath("(//img[@alt='Lookup'])[2]").click();
		return new LookupLead();
	}

	public MergeLeads clickMergeButton() {
		LookupLead.switchToDefaultWindow();
		driver.findElementByXPath("//a[text()='Merge']").click();
		return this;
	}
	
	public ViewLead clickConfirmAlert() {
		Alert alert = driver.switchTo().alert();
		alert.accept();
		return new ViewLead();
	}
}