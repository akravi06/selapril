package pages;

import design.ProjectMethods;

public class MyHomePage extends ProjectMethods {

	public MyLeads clickLeads() {
		driver.findElementByLinkText("Leads").click();
		return new MyLeads();
	}
}